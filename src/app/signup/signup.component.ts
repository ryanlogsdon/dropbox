import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from "../services/auth.service";
import { StateService } from '../services/state.service';

import { MatDialog } from '@angular/material/dialog';

import * as firebase from 'firebase/app';               // TODO: move the firebase current-user check into auth.service.ts
import 'firebase/auth';
import 'firebase/firestore';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    isProgressVisible: boolean;
    signupForm: FormGroup;
    firebaseErrorMessage: string;

    constructor(
        private authService: AuthService,
        private stateService: StateService,
        public dialog: MatDialog,
        private router: Router) {

        this.isProgressVisible = false;

        if (this.stateService.user) {                          // don't allow logged-in users to stay on signup page
            this.router.navigate(['/upload']);
        }

        this.firebaseErrorMessage = '';
    }

    ngOnInit() {
        this.signupForm = new FormGroup({
            'displayName': new FormControl(null, Validators.required),
            'email': new FormControl(null, [Validators.required, Validators.email]),
            'password': new FormControl(null, Validators.required)
        });
    }

    stopPropagation($event: KeyboardEvent) {                    // Chrome users with LastPass have an error if they try to click the enter key
        $event.preventDefault()
        $event.stopPropagation()
    }

    signup() {
        if (this.signupForm.invalid)                            // if there's an error in the form, don't submit it
            return;

        this.isProgressVisible = true;
        this.authService.signupUser(this.signupForm.value).then((result) => {
            if (result == null)                                 // null is success, false means there was an error
                this.router.navigate(['/upload']);
            else if (result.isValid == false)
                this.firebaseErrorMessage = result.message;

            this.isProgressVisible = false;                     // no matter what, when the auth service returns, we hide the progress indicator
        }).catch(() => {
            this.isProgressVisible = false;
        });
    }
}
