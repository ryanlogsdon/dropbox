import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploaderManagerComponent } from './uploader-manager.component';

describe('UploaderManagerComponent', () => {
  let component: UploaderManagerComponent;
  let fixture: ComponentFixture<UploaderManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploaderManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploaderManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
