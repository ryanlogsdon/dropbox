import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service'

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

    isProgressVisible: boolean;

    constructor(private authService: AuthService, private router: Router) { 
        this.isProgressVisible = false;
    }

    ngOnInit(): void {
        this.logout();
    }

    logout(): void {
        console.log('logging out...');
        this.isProgressVisible = true;                          // show the progress indicator as we start the Firebase login process
        this.authService.logoutUser().then((result) => {
            this.isProgressVisible = false;                     // no matter what, when the auth service returns, we hide the progress indicator
        });
    }
}
