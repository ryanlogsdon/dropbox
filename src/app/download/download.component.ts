import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { StateService } from '../services/state.service';

@Component({
    selector: 'app-download',
    templateUrl: './download.component.html',
    styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {

    isProgressVisible: boolean;
    resultFiles: any[];                                        // query results for uploaded files
    displayedColumnsFiles: string[];

    constructor(
        private storage: AngularFireStorage,
        public stateService: StateService) {

        this.isProgressVisible = false;
        this.resultFiles = [];
        this.displayedColumnsFiles = ['User', 'UploadTime', 'DownloadURL', 'Delete'];
    }

    ngOnInit(): void {
        this.findFiles()
    }

    findFiles(): void {
        console.log('finding files...');

        this.resultFiles = [];
        this.isProgressVisible = true;

        let query = this.stateService.firebase.firestore().collection('files');

        let self = this;
        query
            .get()
            .then(function (querySnapshot) {
                self.isProgressVisible = false;

                querySnapshot.forEach(function (doc) {

                    var timestamp = doc.data().timestamp;                               // ex: '1598066351161'
                    var d = new Date(timestamp);
                    var dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                    // var formattedTime = d.toLocaleDateString("en-US");               // ex: 9/17/2016
                    var formattedDate = d.toLocaleDateString("en-US", dateOptions);     // ex: Saturday, September 17, 2016

                    self.resultFiles.push({
                        'storagePath': doc.data().storagePath,
                        'originalName': doc.data().originalName,
                        'uploadDate': formattedDate,
                        'userName': doc.data().userName,
                        'userEmail': doc.data().userEmail.toLowerCase(),
                        'downloadURL': doc.data().downloadURL
                    });
                });

                console.log('results', self.resultFiles);
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
            });
    }

    deleteFile(file): void {
        console.log('deleting', file);

        this.isProgressVisible = true;

        let storagePath = file.storagePath;                             // ex: 'uploads/1598066351161_myfile.txt'
        let delimiter = storagePath.indexOf('/');
        let docID = file.storagePath.substring(delimiter + 1);          // ex: '1598066351161_myfile.txt'

        let self = this;
        this.stateService.firebase.firestore().collection('files').doc(docID).delete().then(function () {
            console.log('cloud firestore doc deleted');
            self.storage.ref(storagePath).delete();

            self.resultFiles.find((o, i) => {                           // remove this object from the resultFiles array
                if (o.storagePath === storagePath) {
                    self.resultFiles.splice(i, 1);
                    let cloned = self.resultFiles.slice();              // update the table
                    self.resultFiles = cloned;
                    return true;                                        // stop searching
                }
            });

            self.isProgressVisible = false;
        }).catch(function (error) {
            console.error('Error deleting cloud firestore doc:', error);
            self.isProgressVisible = false;
        });
    }
}
