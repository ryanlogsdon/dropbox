import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { StateService } from '../services/state.service';

@Component({
    selector: 'uploader-task',
    templateUrl: './uploader-task.component.html',
    styleUrls: ['./uploader-task.component.css']
})
export class UploaderTaskComponent implements OnInit {

    @Input() file: File;
    task: AngularFireUploadTask;                                        // this does the uploading for us

    percentage: Observable<number>;
    snapshot: Observable<any>;
    downloadURL: string;
    sizeUnit: string;                                                   // file.size <= 1024 ('B'), else ('KB')

    constructor(
        private storage: AngularFireStorage,
        private db: AngularFirestore,
        private stateService: StateService) {

    }

    ngOnInit(): void {
        this.startUpload();
    }

    startUpload() {
        console.log('upload task staring...');
        console.log(this.file);

        if (this.file.size <= 1024)                                      // upload updates will be listed in KBs
            this.sizeUnit = 'B';
        else
            this.sizeUnit = 'KB';

        let safeName = this.file.name.replace(/([^a-z0-9.]+)/gi, '');   // file name stripped of spaces and special chars
        let timestamp = Date.now();                                     // ex: '1598066351161'
        const uniqueSafeName = timestamp + '_' + safeName;
        const path = 'uploads/' + uniqueSafeName;                       // Firebase storage path
        const ref = this.storage.ref(path);                             // reference to storage bucket

        this.task = this.storage.upload(path, this.file);
        this.percentage = this.task.percentageChanges();                // progress monitoring
        this.snapshot = this.task.snapshotChanges().pipe(               // emits a snapshot of the transfer progress every few hundred milliseconds
            tap(console.log),
            finalize(async () => {                                      // after the observable completes, get the file's download URL
                this.downloadURL = await ref.getDownloadURL().toPromise();

                this.db.collection('files').doc(uniqueSafeName).set({
                    storagePath: path,
                    downloadURL: this.downloadURL,
                    originalName: this.file.name,
                    timestamp: timestamp,
                    userEmail: this.stateService.user.email,
                    userName: this.stateService.user.displayName
                })
                    .then(function () {
                        console.log('document written!');
                    })
                    .catch(function (error) {
                        console.error('Error writing document:', error);
                    });
            }),
        );
    }

    isActive(snapshot) {
        return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
    }
}
