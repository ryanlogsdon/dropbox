import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { Router } from '@angular/router';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class StateService {

    // app-wide variables
    title: string;                                          // title shown after "Cathedral" on the top bar
    firebase: any;                                          // reference to our Firebase DB app
    user: any;                                              // user doc from Firebase 'user' collection
    // backgroundImageURL: any;

    newClientBillingInfo: any;

    constructor(private router: Router) {
        this.user = null;
        console.log('StateService constructor!');

        if (!firebase.apps.length) {                        // NOTE: this is needed only to fix a Firebase bug when there are multiple Firebase dbs
            this.firebase = firebase.initializeApp(environment.firebase);
            this.buildUserInfo();
        }
    }

    buildUserInfo(): void {
        console.log('stateService: buildUserInfo!');

        var self = this;
        this.firebase.auth().onAuthStateChanged(function (user) {   // firebase takes a while to check if user is logged in, so listen to a state change
            console.log('Auth State changed!');

            if (user) {                                         // don't allow logged-in users to stay on login page
                let email = user['email'].toLowerCase();
                console.log('email', email);

                self.firebase.firestore().collection('users').where("email_lower", '==', email)
                    .onSnapshot(querySnapshot => {
                        querySnapshot.forEach(doc => {
                            self.user = {
                                ...doc.data()
                            };
                            console.log('firebase user', user);
                            console.log('self.user', self.user);

                            if (self.user.accountStatus != 'verified')      // don't allow unverified users access to upload data
                                self.router.navigate(['/home']);
                        })
                    })
            }
            else {
                self.user = null;                                         // user has logged out, so clear our user variable
            }
        });
    }
}
