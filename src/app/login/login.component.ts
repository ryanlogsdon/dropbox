import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service'
import { StateService } from '../services/state.service';

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    isProgressVisible: boolean;
    loginForm: FormGroup;
    firebaseErrorMessage: string;

    constructor(
        private authService: AuthService,
        private stateService: StateService,
        private router: Router) {

        this.isProgressVisible = false;

        this.loginForm = new FormGroup({
            'email': new FormControl('', [Validators.required, Validators.email]),
            'password': new FormControl('', Validators.required)
        });

        this.firebaseErrorMessage = '';
    }

    ngOnInit(): void {
    }

    stopPropagation($event: KeyboardEvent) {                    // Chrome users with LastPass have an error if they try to click the enter key
        $event.preventDefault()
        $event.stopPropagation()
    }

    loginUser() {
        this.isProgressVisible = true;                          // show the progress indicator as we start the Firebase login process

        if (this.loginForm.invalid)
            return;

        this.authService.loginUser(this.loginForm.value.email, this.loginForm.value.password).then((result) => {
            this.isProgressVisible = false;                     // no matter what, when the auth service returns, we hide the progress indicator
            if (result == null) {                               // null is success, false means there was an error
             
                console.log('logging in...');
                console.log('user', this.stateService.user);
                this.router.navigate(['/upload']);
            }
            else if (result.isValid == false) {

                console.log('login error....');
                console.log(result);
                this.firebaseErrorMessage = result.message;

            }
        });
    }
}
