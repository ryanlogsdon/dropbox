import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'uploader-manager',
    templateUrl: './uploader-manager.component.html',
    styleUrls: ['./uploader-manager.component.css']
})
export class UploaderManagerComponent implements OnInit {

    isHovering: boolean;
    files: File[] = [];

    constructor() { }

    ngOnInit(): void {
    }

    toggleHover(event: boolean) {
        this.isHovering = event;
    }

    onDrop(files: FileList) {
        console.log('uploaderParent onDrop');

        for (let i = 0; i < files.length; i++) {
            this.files.push(files.item(i));
        }
    }
}
