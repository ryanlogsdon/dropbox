import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service'
import { StateService } from '../services/state.service';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

    isProgressVisible: boolean;                             // Uploader Task will control the progress indicator

    constructor(
        private authService: AuthService,
        public stateService: StateService,
        private router: Router) {

        this.isProgressVisible = false;
    }

    ngOnInit(): void {
    }

}
