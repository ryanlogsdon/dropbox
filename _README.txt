Overview of our Dropbox app using Angular and Firbase
-----------------------------------------------------

    * To run this cloned version:
        * in a terminal, type "npm install"
        * follow the section below called "Setting up the Firebase part of the project"
        * use your new Firebase credentials in src > app > environments > environment.ts
        * in the termina, type "ng serve"
        * in a browser, navigate to "http://localhost:4200"

    * We'll be setting up a simple Angular app that uses Firebase to upload, store, and download documents
    
    * We need to secure the app so only logged-in users have access to upload and download files
    
    * The files will be stored on Firebase > Storage > "uploads" folder

    * The databases of our users info and file metadata will be stored on 
      Firebase > Cloud Firestore > "users" and "files"  (these are nosql databases, so as you Google your way
      through everything, the better term to use is "collection of documents" instead of "database and records")

    * The authenticated users' info is stored at Firebase > Authentication

    * A very helpful (but fast-paced) video: https://www.youtube.com/watch?v=ok7633BVa4Y




Setting up the Angular environment
----------------------------------

node -v                                     # we need at least node v8
npm -v                                      # we need at least npm v6
npm install -g @angular/cli                 # we need to install or update a global copy of Angular CLI, https://cli.angular.io/

mkdir DropboxWithFirebase
cd DropboxWithFirebase


Setting up the Firebase part of the project
-------------------------------------------

    * we're going to use Firebase Storage to store the files and Firebase Cloud Firestore 
      to build a database tracking our uploads and the metadata of each file

    * firebase.google.com > "Go to console" > Add project > "dropfb" >
      Continue > default account > create project > continue > Web ("</>") > 
      Register app (nickname) >  "dropfb" (the name really doesn't matter, but we'll use the
      same name) > Register app (button) > copy the firebaseConfig object and paste it into
      a temporary text file (we'll add this to our Environment later!) > continue to console

    * Develop > Authentication > Sign-in method > (enable email/password)

        now users can signup and login!

    * Develop > Cloud Firestore > Create database > start in test mode > (choose a Cloud Firestore location) > Enable

        now we can build a database to track the file metadata & user info

    * Develop > Storage > Get started > Next > Done

        now we have storage to upload files to!


Setting up the Angular part of the project
------------------------------------------

    * ng new dropbox                        # this will create a directory for our 'dropbox' project and the project files within
        * add Angular routing?  Yes
        * which stylesheet?     CSS

    * cd dropbox

    * open '.editorconfig' and change 'indent_size = 2' from 2 to 4             # friends don't let friends use 2 spaces

    * open another terminal tab and run `ng serve --port 4202`


Remove boilerplate code
-----------------------
    * in src/app/app.component.html, only keep "<router-outlet></router-outlet>"        # you'll find it at the very bottom of the file


Adding components, services, and Firebase authentication & an Angular auth guard
--------------------------------------------------------------------------------
    ng generate component home                              # landing page
    ng generate component login                             # Firebase login page
    ng generate component logout                            # when you navigate to this page, you'll be automatically logged out
    ng generate component signup                            # Firebase signup page
    ng generate component upload                            # drag & drop (upload) files
    ng generate component download                          # search for existing files, download, and delete them
    
    ng generate component uploaderManager                   # parent component that manages all of the files that are dropped on screen
    ng generate component uploaderTask                      # each child task will control the individual file upload
    ng generate directive dropzone                          # physical area for draging and dropping
    
    ng generate service services/auth                       # Firebase authentication
    ng generate service services/state                      # a singleton service to share state between the components!

    ng generate guard services/auth
        * which interface?  CanActivate

    * open `src > app > app-routing.module.ts`
        * import each component
        * add each component to the routes array
        * import `AuthGuard`
        * add the property `canActivate: [AuthGuard]` to any page that needs an authentication check

    * npm install firebase @angular/fire

    * open `src > app > services > auth.guard.ts`
        * import the code

    * open `src > app > services > auth.service.ts`
        * import the code

    * open `src > app > services > state.service.ts`
        * import the code
        * Note: for any new component that you write, in order for it to have access 
          to the State service:
            * import StateService in the component's *.ts file 
            * add StateService to that *.ts file's constructor

    * open `src > environments > environment.ts`
        * put the firebaseConfig snippet from the temporary text file in here



Add Material Design
-------------------
    * Why though?
        * it's not just for style! we need to be able to see the upload progress of our files,
          and this will make it easy as can be :)

    * ng add @angular/material
        * choose a theme        (any)
        * typography            Yes
        * browser animations?   Yes (you can choose "No", but even if you choose "Yes" and end up
          not using it, when you build your "production-ready" app, the unused libraries will be removed)

    * open `src > app > app.module.ts`
        * copy the code, or...

        * import the Material Design modules (there's a lot!) & add them to the imports array (they start with the letters "Mat")
        * import FormsModule and the imports array

        * while we're in the App Module, add the following to our imports:

            FormsModule
            ReactiveFormsModule
            HttpClientModule
            AngularFireModule
            AngularFirestoreModule
            AngularFireDatabaseModule
            AngularFireStorageModule
            environment



Style the pages we'll be creating
---------------------------------
    * copy src/styles.css


Add link on Home to all pages
-----------------------------
    * copy home/home.component.html & .ts
    * copy the Home button to each of the other components (or just copy all the files; see below...)


Enable Signup, Login, and Logout
--------------------------------
    * copy signup/signup.component.html & .ts
    * copy login/login.component.html & .ts
    * copy logout/logout.component.html & .ts


Let's build the Drag & Drop!!!!
-------------------------------
    * copy uploader-task/uploader-task.component.html & .ts
        * UploaderManager is going to use this 'uploader-task' component for every file upload
        * Note: we shortened the 'selector' name from 'app-uploader-task' to 'uploader-task'
        * Note: you can un-comment the HTML code to generate a direct-download link if you want

    * copy uploader-manager/uploader-manager.component.html & .ts
        * Upload is going to use this 'uploader-manager' component
        * Note: we shortened the 'selector' name from 'app-uploader-manager' to 'uploader-manager'
        
    * copy app/dropzone.directive.ts


Enable file search & download
-----------------------------
    * copy download/download.component.html & .ts


Build for Production
--------------------
    * "ng build --prod"

    * Note: this will remove all unused libraries

    * you can take the contents of your "dropbox/dist/dropbox" folder and upload it to your website!

