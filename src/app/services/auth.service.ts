import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

import { StateService } from './state.service';

import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private stateService: StateService,
        private router: Router) {

    }

    loginUser(email: string, password: string): Promise<any> {
        return this.stateService.firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => {
                console.log('authService: loginUser: success');
                // this.router.navigate(['/uploadData']);
            })
            .catch(error => {
                console.log('auth login error...');
                console.log(error.code);
                console.log(error)
                if (error.code)
                    return { isValid: false, message: error.message };
            });
    }

    signupUser(user: any): Promise<any> {
        console.log("auth service");
        console.log(user);

        return firebase
            .auth()
            .createUserWithEmailAndPassword(user.email, user.password)
            .then((newUserCredential: firebase.auth.UserCredential) => {

                let emailLower = user.email.toLowerCase();

                firebase
                    .firestore()
                    .doc('/users/' + emailLower)
                    .set({
                        accountStatus: 'unverified',
                        accountType: 'endUser',
                        displayName: user.displayName,
                        displayName_lower: user.displayName.toLowerCase(),
                        email: user.email,
                        email_lower: emailLower
                    });
            })
            .catch(error => {
                console.log('auth signup error...');
                if (error.code)
                    return { isValid: false, message: error.message };
            });
    }

    resetPassword(/*email: string*/): Promise<any> {
        let email = this.stateService.user.email;
        return firebase.auth().sendPasswordResetEmail(email)
            .then(() => {
                // this.router.navigate(['/amount']);
            })
            .catch(error => {
                console.log('error.code...');
                console.log(error.code);
                console.log(error)
                if (error.code)
                    return error;
            });
    }

    logoutUser(): Promise<void> {
        return firebase.auth().signOut();
    }

    setUserInfo(payload: object) {
        console.log("saving user in user service")
        firebase.firestore().collection("users")
            .add(payload).then(function (res) {
                console.log("response from user service completion")
                console.log(res);
            })
    }

    //if user is logged in - returns user object from firebase
    //if user is NOT logged in - returns null from firebase
    getCurrentUser() {
        return firebase.auth().currentUser;
    }
}
